    const video     = document.querySelector('#videoElement');

    navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia || navigator.oGetUserMedia;

    if (navigator.getUserMedia) {
      console.log(navigator);
      navigator.mediaDevices.getUserMedia({ video: true })
        .then(handleVideo)
        .catch(videoError);
    }

    function handleVideo(stream) {
      video.srcObject = stream;
      video.onloadedmetadata = () => {
        video.play();
      };
    }

    function videoError(e) {
      console.error(e);
    }

    function onSelect(e) {
      localStorage.setItem('diameter', e.value);
      document.getElementById('circle').className = e.value;
    }

    const baseWidth   = video.clientWidth / 2;
    const baseHeight  = video.clientHeight / 2;

    let xDelta = 0;
    let yDelta = 0;

    function setupCenter() {
      xDelta = +localStorage.getItem('xDelta') || 0;
      yDelta = +localStorage.getItem('yDelta') || 0;

      document.querySelector('.horline').style.height = `${baseHeight + yDelta}px`;
      document.querySelector('.vertline').style.width = `${baseWidth + xDelta}px`;
      document.querySelector('#circle').style.top = `${baseHeight + yDelta}px`;
      document.querySelector('#circle').style.left = `${baseWidth + xDelta}px`;
    }

    function handleWheel(e) {
      if (e.altKey) {
        localStorage.setItem('xDelta', `${xDelta + (e.deltaY > 0 ? 1 : -1)}`);
      } else {
        localStorage.setItem('yDelta', `${yDelta + (e.deltaY < 0 ? 1 : -1)}`);
      }

      setupCenter();
    }

    window.setTimeout(() => {
      const diameter = localStorage.getItem('diameter');

      if (diameter) {
        document.getElementById('circle').className = diameter;
        document.getElementById('selector').value = diameter;
      }

      setupCenter();
    }, 100);
    document.addEventListener('wheel', handleWheel);
