#include "motor.h"
#include "defs.h"
//-----------------------------------------------------------------------------
cMotor motor;
//-----------------------------------------------------------------------------
void cMotor::init()
{
    clr_bit(CONFIG_MOTOR_PORT,CONFIG_MOTOR_PIN);
    set_bit(CONFIG_MOTOR_DDR,CONFIG_MOTOR_PIN);
}
//-----------------------------------------------------------------------------
void cMotor::on()
{
    set_bit(CONFIG_MOTOR_PORT,CONFIG_MOTOR_PIN);
}
//-----------------------------------------------------------------------------
void cMotor::off()
{
    clr_bit(CONFIG_MOTOR_PORT,CONFIG_MOTOR_PIN);
}
//-----------------------------------------------------------------------------
