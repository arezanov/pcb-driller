//-----------------------------------------------------------------------------
// cpedal.cpp
//-----------------------------------------------------------------------------
#include "defs.h"
#include "sensors.h"
//-----------------------------------------------------------------------------
cPedal pedal;
cEndSwitch finish;
//-----------------------------------------------------------------------------
void cPedal::init()
{
    clr_bit(CONFIG_PEDAL_DDR,CONFIG_PEDAL_PIN);
    set_bit(CONFIG_PEDAL_PORT,CONFIG_PEDAL_PIN);    // подтягиваем к 1
}
//-----------------------------------------------------------------------------
BYTE cPedal::is_pressed()
{
    if(test_bit(CONFIG_PEDAL_INPUT,CONFIG_PEDAL_PIN))
    {
        return 0;
    }
    return 1;
}
//=============================================================================
void cEndSwitch::init()
{
    set_bit(CONFIG_ENDG_DDR,CONFIG_ENDG_PIN);
    set_bit(CONFIG_END_PORT,CONFIG_END_PIN);
}
//-----------------------------------------------------------------------------
BYTE cEndSwitch::is_pressed()
{
    if(test_bit(CONFIG_END_INPUT,CONFIG_END_PIN))
    {
        return 0;
    }
    return 1;
}
//-----------------------------------------------------------------------------
