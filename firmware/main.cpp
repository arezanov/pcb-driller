//-----------------------------------------------------------------------------
#include "defs.h"
#include "motor.h"
#include "servo.h"
#include "sensors.h"
//----------------------------------------------------------------------------
//------------------------------- BSS section --------------------------------
//-----------------------------------------------------------------------------
extern cMotor motor;
extern cServo servo;
extern cPedal pedal;
extern cEndSwitch finish;
//-----------------------------------------------------------------------------
#define CARET_PARK      14
#define CARET_PREPARE   28
#define CARET_FIN       34
//-----------------------------------------------------------------------------
int main()
{
    mdelay(300);    // для самоинициализации сервопривода
    pedal.init();
    finish.init();
    motor.init();
    servo.init(CARET_PARK);
    motor.on();
    mdelay(50);
    //motor.off();
    while(1);
    {
        if(pedal.is_pressed())
        {
            mdelay(20); // подавление дребезга контактов
            if(pedal.is_pressed())
            {
                motor.on();
                servo.set(CARET_PREPARE);
                mdelay(600);    // задержка на затухание колебаний после резкого подъема
                for(BYTE i=CARET_PREPARE+1;i<=CARET_FIN;i++)
                {
                    // плавно подаем, натягивая пружину
                    servo.set(i);
                    mdelay(100);
                }
                while(!finish.is_pressed());  // ждем сработку концевика
                servo.set(CARET_PARK);
                mdelay(100);    // задержка на выход вращающегося сверла
                motor.off();
            }
        }
        mdelay(10);
    }
    return 0;
}
//-----------------------------------------------------------------------------
