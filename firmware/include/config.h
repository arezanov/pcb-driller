//----------------------------------------------------------------------------
// config.h
//----------------------------------------------------------------------------
#ifndef CONFIG_H
#define CONFIG_H
//----------------------------------------------------------------------------
#include <avr/io.h>
//----------------------------------------------------------------------------

// pedal input on arduino 4
#define CONFIG_PEDAL_PORT   PORTD
#define CONFIG_PEDAL_INPUT  PIND
#define CONFIG_PEDAL_DDR    DDRD
#define CONFIG_PEDAL_PIN    4
// end switch send on arduino 6
#define CONFIG_ENDG_PORT    PORTD
#define CONFIG_ENDG_DDR     DDRD
#define CONFIG_ENDG_PIN     6
// end switch receive 7
#define CONFIG_END_PORT    PORTD
#define CONFIG_END_INPUT   PIND
#define CONFIG_END_DDR     DDRD
#define CONFIG_END_PIN     7
// caret servo on arduino 3
#define CONFIG_SERVO_PORT   PORTD
#define CONFIG_SERVO_DDR    DDRD
#define CONFIG_SERVO_PIN    3
// motor on arduino A0
#define CONFIG_MOTOR_PORT   PORTC
#define CONFIG_MOTOR_DDR    DDRC
#define CONFIG_MOTOR_PIN    0
//----------------------------------------------------------------------------
#endif
