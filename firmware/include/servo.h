#ifndef SERVO_H
#define SERVO_H
//-----------------------------------------------------------------------------
class cServo
{
public:
    void init(BYTE pos);
    void set(BYTE pos);
};
//-----------------------------------------------------------------------------
#endif
