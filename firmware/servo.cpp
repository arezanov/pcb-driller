#include "defs.h"
#include "servo.h"
//-----------------------------------------------------------------------------
cServo servo;
//-----------------------------------------------------------------------------
void cServo::init(BYTE pos=0)
{
    TCCR2A=bit(COM2B1) |    // Clear OC2B on Compare Match, set OC2B at BOTTOM (non-inverting mode).
           bit(WGM20) | bit(WGM21);             // fast PWM mode with 0xFF top
    TCCR2B=bit(CS20) | bit(CS21) | bit(CS22);   // 1024 prescaler
    OCR2B=pos;
    set_bit(CONFIG_SERVO_DDR,CONFIG_SERVO_PIN);
}
//-----------------------------------------------------------------------------
void cServo::set(BYTE pos)
{
    OCR2B=pos;
}
//-----------------------------------------------------------------------------
